package cl.duoc.torneofutbolito;

/**
 * Created by harttynarce on 09-09-16.
 */
public class Jugador {
    private String rut;
    private String nombre;
    private String apodo;
    private int nroCamiseta;
    private String posicion;
    private String fechaNacimiento;

    public Jugador(String rut, String nombre, String apodo, int nroCamiseta, String posicion, String fechaNacimiento) {
        this.rut = rut;
        this.nombre = nombre;
        this.apodo = apodo;
        this.nroCamiseta = nroCamiseta;
        this.posicion = posicion;
        this.fechaNacimiento = fechaNacimiento;
    }

    public Jugador() {

    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApodo() {
        return apodo;
    }

    public void setApodo(String apodo) {
        this.apodo = apodo;
    }

    public int getNroCamiseta() {
        return nroCamiseta;
    }

    public void setNroCamiseta(int nroCamiseta) {
        this.nroCamiseta = nroCamiseta;
    }

    public String getPosicion() {
        return posicion;
    }

    public void setPosicion(String posicion) {
        this.posicion = posicion;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }
}
