package cl.duoc.torneofutbolito;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class AgregarJugadorActivity extends AppCompatActivity {

    private Spinner spnEquipo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_jugador);

        spnEquipo = (Spinner)findViewById(R.id.spnEquipo);
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, BD.getInstance().getNombresDeEquipos()); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnEquipo.setAdapter(spinnerArrayAdapter);
    }
}
