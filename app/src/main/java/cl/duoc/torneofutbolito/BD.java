package cl.duoc.torneofutbolito;

import java.util.ArrayList;

/**
 * Created by harttynarce on 09-09-16.
 */
public class BD {
    private static BD instance;
    private static ArrayList<Equipo> equipos = new ArrayList<>();

    private BD(){}

    public static BD getInstance(){
        if(instance == null){
            instance = new BD();
        }
        return instance;
    }

    public void agregarEquipo(Equipo equipo){
        equipos.add(equipo);
    }

    public Equipo buscarEquipoPorNombre(String nombreEquipo){
        for(int x = 0 ; x < equipos.size(); x++){
            if(equipos.get(x).getNombre().equals(nombreEquipo)){
                return equipos.get(x);
            }
        }
        return null;
    }

    public void agregarJugador(String nombreEquipo,
                               Jugador nuevoJugador){

        Equipo equipo = buscarEquipoPorNombre(nombreEquipo);
        if(equipo != null){
            equipo.agregarJugador(nuevoJugador);
        }
    }

    public ArrayList<String> getNombresDeEquipos(){
        ArrayList<String> nombres = new ArrayList<>();
        for (Equipo aux : equipos) {
            nombres.add(aux.getNombre());
        }
        return nombres;
    }


}
