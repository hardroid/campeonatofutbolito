package cl.duoc.torneofutbolito;

import java.util.ArrayList;

/**
 * Created by harttynarce on 09-09-16.
 */
public class Equipo {
    private String uid;
    private String nombre;
    private String fechaCreacion;
    private String urlInsignia;
    private String colorCamist;
    private String nombreEntrenador;
    private ArrayList<Jugador> jugadores;

    public Equipo() {
        jugadores = new ArrayList<>();
    }

    public Equipo(String uid, String nombre, String fechaCreacion, String urlInsignia, String colorCamist, String nombreEntrenador) {
        this.uid = uid;
        this.nombre = nombre;
        this.fechaCreacion = fechaCreacion;
        this.urlInsignia = urlInsignia;
        this.colorCamist = colorCamist;
        this.nombreEntrenador = nombreEntrenador;
        jugadores = new ArrayList<>();
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUrlInsignia() {
        return urlInsignia;
    }

    public void setUrlInsignia(String urlInsignia) {
        this.urlInsignia = urlInsignia;
    }

    public String getColorCamist() {
        return colorCamist;
    }

    public void setColorCamist(String colorCamist) {
        this.colorCamist = colorCamist;
    }

    public String getNombreEntrenador() {
        return nombreEntrenador;
    }

    public void setNombreEntrenador(String nombreEntrenador) {
        this.nombreEntrenador = nombreEntrenador;
    }

    public void agregarJugador(Jugador nuevoJugador){
        jugadores.add(nuevoJugador);
    }
}
