package cl.duoc.torneofutbolito;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button btnInsJugador, btnCrearEquipo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnInsJugador = (Button)findViewById(R.id.btnInsJugador);
        btnCrearEquipo = (Button)findViewById(R.id.btnCrearEquipo);

        btnInsJugador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, AgregarJugadorActivity.class);
                startActivity(i);
            }
        });

        btnCrearEquipo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, AgregarEquipoActivity.class);
                startActivity(i);
            }
        });
    }
}
