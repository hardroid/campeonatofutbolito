package cl.duoc.torneofutbolito;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class AgregarEquipoActivity extends AppCompatActivity {

    private EditText txtNombre, txtFechaCreacion, txtUrlImagen, txtEntrenador,
            txtColorCamiseta;

    private Button btnAgregarEquipo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_equipo);

        txtNombre = (EditText)findViewById(R.id.txtNombre);
        txtFechaCreacion = (EditText)findViewById(R.id.txtFechaCreacion);
        txtUrlImagen = (EditText)findViewById(R.id.txtUrlImagen);
        txtEntrenador = (EditText)findViewById(R.id.txtEntrenador);
        txtColorCamiseta = (EditText)findViewById(R.id.txtColorCamiseta);

        btnAgregarEquipo = (Button)findViewById(R.id.btnAgregarEquipo);

        btnAgregarEquipo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                crearEquipo();
            }
        });

    }

    private void crearEquipo() {
        Equipo nuevoEquipo = new Equipo("1",
                txtNombre.getText().toString(),
                txtFechaCreacion.getText().toString(),
                txtUrlImagen.getText().toString(),
                txtColorCamiseta.getText().toString(),
                "Loco Bielsa");

        BD.getInstance().agregarEquipo(nuevoEquipo);
    }
}
